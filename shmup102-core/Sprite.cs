using System;
using Microsoft.Xna.Framework;

namespace shmup102_core
{
    public class Sprite : IDrawable
    {   
        public string textureName {get;set;}
        public Vector2 position {get;set;}
        public int spriteIdx {get;set;}
        public float scale { get; set; } =1;

        public Sprite(Vector2 initialPosition, string textureName, int spriteIdx){
            this.position = initialPosition;
            this.textureName = textureName;
            this.spriteIdx = spriteIdx;
        }
    }
}
