using System;
using Microsoft.Xna.Framework;
using BulletMLLib;

namespace shmup102_core
{
    public class BaseBullet : Bullet, IDrawable
    {   
        public string textureName {get;set;} ="purp_diamond_bullet";
        public Vector2 position {get;set;}
        public int spriteIdx {get;set;} = 0;
        public override float X { get; set; }
        public override float Y { get; set; }
        public bool used {get;set;}
        public float scale { get; set;} =.2f;

        public BaseBullet(IBulletManager bulletManager): base(bulletManager){
            
        }

        public void Init(){
            used = true;
        }

        public override void PostUpdate()
        {
            this.position = new Vector2(X,Y);
            if (X < CoreShmup.playArea.Left || X > CoreShmup.playArea.Right 
            || Y < CoreShmup.playArea.Top || Y > CoreShmup.playArea.Bottom)
			{
				used = false;
			}
        }
    }
}
