using System;
using Microsoft.Xna.Framework;

namespace shmup102_core
{
    public class Player : IDrawable
    {   
        public string textureName {get;set;}
        public Vector2 position {get;set;}
        public int spriteIdx {get;set;}
        public float scale { get; set; } =1;

        public float speed = 250;
        private BulletSource bulletSource = null;
        public Player(Vector2 initialPosition, string textureName){
            this.position = initialPosition;
            this.textureName = textureName;
            this.spriteIdx = 0; //TODO this should be abstracted . should be sprite idx?
        }

        public void SetBulletPattern(BulletMLLib.BulletPattern pattern){
            if(bulletSource == null)
                this.bulletSource = new BulletSource(pattern,this.position);     
        }

        public void Move(InputData inputData, TimeSpan deltaTime){
            float x = 0;
            float y = 0;
            if(inputData.xAxis > 0){
                x -= this.speed * (float) deltaTime.TotalSeconds;
            } else if (inputData.xAxis < 0){
                x += this.speed * (float) deltaTime.TotalSeconds;
            }
            if(inputData.yAxis > 0){
                y -= this.speed * (float) deltaTime.TotalSeconds;
            }else if(inputData.yAxis < 0){
                y += this.speed * (float) deltaTime.TotalSeconds;
            }
            this.position += new Vector2(x,y);
        }

        public void ClampPositionToSafeArea(){
            if(this.position.Y < CoreShmup.safePlayArea.Top){
                this.position = new Vector2(this.position.X,CoreShmup.safePlayArea.Top);
            } else if (this.position.Y > CoreShmup.safePlayArea.Bottom){
                this.position = new Vector2(this.position.X,CoreShmup.safePlayArea.Bottom);
            } 
            if(this.position.X < CoreShmup.safePlayArea.Left){
                this.position = new Vector2(CoreShmup.safePlayArea.Left,this.position.Y);
            } else if (this.position.X > CoreShmup.safePlayArea.Right){
                this.position = new Vector2(CoreShmup.safePlayArea.Right,this.position.Y);
            } 
        }

        public void Update(TimeSpan deltaTime, InputData inputData){
            this.Move(inputData,deltaTime);
            this.ClampPositionToSafeArea();
            
            if(bulletSource != null)
                this.bulletSource.Update(this.position); // maybe should be in set of this position


        }
    }
}
