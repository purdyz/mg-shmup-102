﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;

namespace shmup102_core
{
    public class CoreShmup
    {
        public const int width = 1280;
        public const int height = 720;
        public static Rectangle playArea = new Rectangle(370,0,540,720);//new Rectangle(370,0,910,720); //play area is in the middle 540x720 classic 3:4
        public static Rectangle safePlayArea = new Rectangle(402,32,476,656);//new Rectangle(370,0,910,720); //play area is in the middle 540x720 classic 3:4

        public Dictionary<string,BulletMLLib.BulletPattern> bulletmlPatterns = new Dictionary<string, BulletMLLib.BulletPattern>();
        List<Sprite> backgroundSprites = new List<Sprite>();
        private float difficultyRank=0.1f;

        Player player;

        //TODO CoreShmup Should probably take a configuration class. Which would likely hold all the configurations for textures names and enemies and levels and everything...
        public CoreShmup(string bulletmlPatternDirectory){
            LoadBulletMLPatterns(bulletmlPatternDirectory);
            player = new Player(new Vector2(playArea.Center.X,playArea.Center.Y), "ship1");
            player.SetBulletPattern(bulletmlPatterns["base_player_pattern1.xml"]);

            backgroundSprites.Add(new Sprite(new Vector2(playArea.Left/2,playArea.Height/2), "sideBackground1",0));
            backgroundSprites.Add(new Sprite(new Vector2(playArea.Right+(playArea.Left/2),playArea.Height/2), "sideBackground1",0));
            //Here is some bulletMLLib setup
            BulletMLLib.GameManager.GameDifficulty = this.GetDifficulty;
            BulletManager.GetInstance().playerPositionDelegate = this.GetPlayerPosition;

        }

        public float GetDifficulty() { return difficultyRank; }
        public Vector2 GetPlayerPosition() {return player.position;}
        public List<IDrawable> Update(TimeSpan elapsedTime, InputData inputData){
            var drawables = new List<IDrawable>();
            //Update Player
            player.Update(elapsedTime,inputData);
            //Update all bullets
            BulletManager.GetInstance().Update();

            drawables.AddRange(backgroundSprites);
            drawables.Add(player);
            drawables.AddRange(BulletManager.GetInstance().bullets);
            return drawables;
        }

        public void LoadBulletMLPatterns(string bulletmlPatternDirectory){
            //Get all the xml files in the Content\\BulletMLScripts directory
			foreach (var source in Directory.GetFiles(bulletmlPatternDirectory, "*.xml"))
			{
                try{
				    //load the pattern
                    BulletMLLib.BulletPattern pattern = new BulletMLLib.BulletPattern();
                    pattern.ParseXML(source);
                    bulletmlPatterns.Add(Path.GetFileName(source),pattern);
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Exception caught while parsing bullet pattern {1}.", e,source);
                }
			}
        }
    }
}
