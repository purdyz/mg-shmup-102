using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace shmup102_app
{
    public class TextureData
    {   
        public string textureName {get;set;}
        public Vector2 spriteSize {get;set;}
        public Texture2D texture {get;set;}

        public TextureData(string textureName,Vector2 spriteSize, Texture2D texture){
            this.textureName=textureName;
            this.spriteSize=spriteSize;
            this.texture=texture;
        }
    }
}
